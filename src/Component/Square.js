import { Grid, Paper, Typography } from "@mui/material";
import React, { useState } from "react";

const Square = ({ numOfSqr, changeTurn, turn, changeArrayBoard }) => {
  const [sqrValue, setSqrValue] = useState("");
  const [markAsDone, setMarkAsDone] = useState(false);

  const onSqureClick = () => {
    if (!markAsDone) {
      turn ? setSqrValue("X") : setSqrValue("O");
      setMarkAsDone(true);
      changeTurn();
      changeArrayBoard(numOfSqr);
    }
  };

  return (
    <Grid item sx={{ color: "white" }}>
      <Paper
        key={numOfSqr}
        sx={{
          width: "5rem",
          height: "5rem",
          border: "4px solid ",
          borderColor: "gray",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
        onClick={() => onSqureClick()}>
        <Typography sx={{ fontSize: "4rem" }}>{sqrValue}</Typography>
      </Paper>
    </Grid>
  );
};

export default Square;
