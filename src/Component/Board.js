import { Grid, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import Square from "./Square";

const Board = () => {
  const [turn, setTurn] = useState(true);
  const [arrayBoard, setArrayBoard] = useState(new Array(9).fill(null));
  let a = [0, 1, 2, 3, 4, 5, 6, 7, 8];
  const changeTurn = () => {
    setTurn((prv) => !prv);
  };

  const changeArrayBoard = (numOfSqr) => {
    let valueOfSqr;
    turn ? (valueOfSqr = 0) : (valueOfSqr = 1);
    let newArray = [...arrayBoard];
    newArray.splice(numOfSqr, 1, valueOfSqr);
    setArrayBoard(newArray);
  };
  // Checking for winner
  let beWinner = arrayBoard.filter((item) => item !== null);

  const checkIfWinner = (i, j, k) => {
    if (i !== null) {
      if (i === j) {
        if (i === k) {
          let player = "";
          i ? (player = "O") : (player = "X");
          alert(`The Winner is ${player}`);
        }
      }
    }
  };

  useEffect(() => {
    if (beWinner.length >= 5) {
      console.log("maybe winner");
      console.log(arrayBoard);
      checkIfWinner(arrayBoard[0], arrayBoard[1], arrayBoard[2]);
      checkIfWinner(arrayBoard[3], arrayBoard[4], arrayBoard[5]);
      checkIfWinner(arrayBoard[6], arrayBoard[7], arrayBoard[8]);
      checkIfWinner(arrayBoard[0], arrayBoard[3], arrayBoard[6]);
      checkIfWinner(arrayBoard[1], arrayBoard[4], arrayBoard[7]);
      checkIfWinner(arrayBoard[2], arrayBoard[5], arrayBoard[8]);
      checkIfWinner(arrayBoard[0], arrayBoard[4], arrayBoard[8]);
      checkIfWinner(arrayBoard[2], arrayBoard[4], arrayBoard[6]);
      let found = arrayBoard.some((i) => i === null);
      if (!found) {
        alert("Draw");
      }
    }
  }, [beWinner.length, arrayBoard]);

  return (
    <>
      <Grid container sx={{ height: "1rem" }}>
        <Grid
          item
          md={12}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}>
          <Typography sx={{ fontSize: "5rem", color: turn ? "blue" : "green" }}>
            {turn ? "X Turn" : "O Turn"}
          </Typography>
        </Grid>
      </Grid>
      <Grid
        container
        sx={{ width: "18rem", hight: "25rem" }}
        direction="row"
        rowGap={4}>
        {a.map((item, index) => {
          return (
            <Grid item md={4} key={index}>
              <Square
                key={index}
                numOfSqr={item}
                turn={turn}
                changeTurn={changeTurn}
                changeArrayBoard={changeArrayBoard}
              />
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

export default Board;
