import "./App.css";
import Board from "./Component/Board";
import { Grid } from "@mui/material";

function App() {
  return (
    <Grid
      container
      sx={{
        backgroundColor: "#1A1A1A",
        height: "100vh",
        width: "100vw",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}>
      <Board />
    </Grid>
  );
}

export default App;
